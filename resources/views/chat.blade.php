<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Chat</title>
    <style>
        .list-group{
            overflow-y: auto;
            white-space:normal;
            height: 200px;
        }
        .list-group span{
            /*width: 100%;*/
            /*max-width: 100%;*/
            /*overflow: auto;*/
            /*resize: vertical;*/
            /*flex-direction: column;*/
            /*resize: auto;*/
            /*cursor: auto;*/
            /*white-space: pre-wrap;*/
            word-wrap: break-word;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    {{-- <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/0.12.14/vue.min.js"></script> --}}
</head>
<body>
<div class="container">
    <div class="row" id="app">
        <div class=" offset-md-4 col-md-4 offset-sm-1 col-sm-10">
            <li class="list-group-item active ">Chat Room <span class="badge badge-pill badge-warning">@{{ numberOfUsers }}</span>
                <div class="badge badge-info">@{{ typing }}</div>
            </li>

            <ul class="list-group" v-chat-scroll >
                <message v-for="value,index in chat.message"
                :key="value.index"
                :color=chat.color[index]
                :user= chat.user[index]
                :time= chat.time[index]
                >
                <span>@{{ value }}</span>
                </message>
            </ul>
                <input type="text" class="form-control" placeholder="type your message ..." v-model="message"
                       @keyup.enter="send"> <br>
        <a href='' class="btn btn-sm btn-danger " @click.prevent='deleteSession'>Delete Chats</a>

        </div>
    </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>